import TicTacToeTemplate from "./template.html";

function createTicTacToeElement() {
  const templateWrapper = document.createElement("template");
  templateWrapper.innerHTML = TicTacToeTemplate;

  const component$ = templateWrapper.content.firstElementChild.cloneNode(true);
  return component$;
}

export { createTicTacToeElement };
