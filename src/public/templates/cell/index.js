import cellTemplate from "./template.html";

function createCellElement() {
  const templateWrapper = document.createElement("template");
  templateWrapper.innerHTML = cellTemplate;

  const cell$ = templateWrapper.content.firstElementChild.cloneNode(true);

  return cell$;
}

export { createCellElement };
