import { createCellElement } from "./public/templates/cell";
import { store, X_TURN, O_TURN } from "./store.js";

function startNewGameHandler() {
  store.finishingData = { isFinished: false, winner: false };
  store.cells = store.cells.map((currentCell) => ({
    ...currentCell,
    value: null,
  }));
  store.currentTurn = X_TURN;
}

function initCells() {
  const cells = new Array(store.rowCount * store.rowCount).fill(null);

  store.cells = cells.map(() => ({
    element: createCellElement(),
    value: null,
  }));
}

function initGame(target) {
  store.appTarget = target;
  initCells();
}

function changeCurrentTurn() {
  if (store.currentTurn === X_TURN) {
    return (store.currentTurn = O_TURN);
  }

  return (store.currentTurn = X_TURN);
}

function checkFinishingGame(cells) {
  const arrayOfCellsValues = cells.map((cell) => cell.value);
  const zeroCellValue = arrayOfCellsValues[0];
  const firstCellValue = arrayOfCellsValues[1];
  const secondCellValue = arrayOfCellsValues[2];
  const thirdCellValue = arrayOfCellsValues[3];
  const sixCellValue = arrayOfCellsValues[6];

  if (zeroCellValue) {
    if (
      Boolean(
        (zeroCellValue === arrayOfCellsValues[3] &&
          zeroCellValue === arrayOfCellsValues[6]) ||
          (zeroCellValue === arrayOfCellsValues[1] &&
            zeroCellValue === arrayOfCellsValues[2]) ||
          (zeroCellValue === arrayOfCellsValues[4] &&
            zeroCellValue === arrayOfCellsValues[8])
      )
    ) {
      return {
        isFinished: true,
        winner: zeroCellValue,
      };
    }
  }

  if (
    firstCellValue &&
    Boolean(
      firstCellValue === arrayOfCellsValues[4] &&
        firstCellValue === arrayOfCellsValues[7]
    )
  ) {
    {
      return {
        isFinished: true,
        winner: firstCellValue,
      };
    }
  }

  if (secondCellValue) {
    if (
      Boolean(
        (secondCellValue === arrayOfCellsValues[5] &&
          secondCellValue === arrayOfCellsValues[8]) ||
          (secondCellValue === arrayOfCellsValues[4] &&
            secondCellValue === arrayOfCellsValues[6])
      )
    ) {
      return {
        isFinished: true,
        winner: secondCellValue,
      };
    }
  }

  if (
    thirdCellValue &&
    Boolean(
      thirdCellValue === arrayOfCellsValues[4] &&
        thirdCellValue === arrayOfCellsValues[5]
    )
  ) {
    {
      return {
        isFinished: true,
        winner: thirdCellValue,
      };
    }
  }

  if (
    sixCellValue &&
    Boolean(
      sixCellValue === arrayOfCellsValues[7] &&
        sixCellValue === arrayOfCellsValues[8]
    )
  ) {
    {
      return {
        isFinished: true,
        winner: sixCellValue,
      };
    }
  }

  if (arrayOfCellsValues.every((cellValue) => cellValue)) {
    return {
      isFinished: true,
      winner: null,
    };
  }

  return { isFinished: false, winner: null };
}

function cellMarkedHandler(markedCell) {
  if (store.finishingData.isFinished) {
    return;
  }

  const foundCellIndex = store.cells.findIndex(
    (cell) => cell.element === markedCell.element
  );

  const foundCell = store.cells[foundCellIndex];

  if (foundCell.value) {
    return;
  }

  foundCell.value = store.currentTurn;

  const finishingGameResult = checkFinishingGame(store.cells);

  if (finishingGameResult.isFinished) {
    store.finishingData = finishingGameResult;
  }

  changeCurrentTurn();
}

export { initGame, startNewGameHandler, cellMarkedHandler };
