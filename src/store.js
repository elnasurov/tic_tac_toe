import { render } from "./renderer.js";

const X_TURN = "X";
const O_TURN = "O";

let store = {
  appTarget: null,
  currentTurn: X_TURN,
  finishingData: {
    isFinished: false,
    winner: null,
  },
  rowCount: 3,
  cells: [],
};

store = new Proxy(store, {
  get(target, prop) {
    return target[prop];
  },
  set(storeTarget, prop, newValue) {
    if (prop === "cells") {
      storeTarget[prop] = newValue.map(
        (currentCell) =>
          new Proxy(currentCell, {
            get(target, prop) {
              return target[prop];
            },
            set(target, prop, newValue) {
              target[prop] = newValue;
              render(storeTarget);
              return true;
            },
          })
      );
    } else {
      storeTarget[prop] = newValue;
    }
    render(storeTarget);
    return true;
  },
});

export { store, X_TURN, O_TURN };
