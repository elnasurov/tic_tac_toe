import { createTicTacToeElement } from "./public/templates/TicTacToe";
import { startNewGameHandler, cellMarkedHandler } from "./services.js";
import { X_TURN, O_TURN } from "./store.js";

const TIC_TAC_TOE_GAME_BOARD_SELECTOR = "[data-game-board]";
const START_NEW_GAME_BUTTON_SELECTOR = "[data-start-new-game]";
const CURRENT_TURN_SELECTOR = "[data-active-step]";
const TIC_TAC_TOE_DISABLED_CLASS = "tic_tac_toe--disabled";
const WINNER_BLOCK_SELECTOR = "[data-winner-block]";

let rootElement$ = null;
let currentTurnElement$ = null;
let winnerElement$ = null;
let gameBoardElement$ = null;
let startNewGameButton$ = null;

let currentState = null;

function getCorrectClassByCurrentTurn(currentTurn) {
  if (currentTurn === X_TURN) {
    return "sign--X";
  }

  if (currentTurn === O_TURN) {
    return "sign--O";
  }
}

function initNewStartGameButtonListener(store) {
  startNewGameButton$.addEventListener("click", () => {
    unsetFinishingBLock();
    setGameBoardEnabled();
    startNewGameHandler();
    setActiveTurn(store.currentTurn);
  });
}

function updateCellView(cell, X_TURN, O_TURN) {
  if (!cell.value) {
    cell.element.classList.remove(getCorrectClassByCurrentTurn(X_TURN));
    cell.element.classList.remove(getCorrectClassByCurrentTurn(O_TURN));
    return;
  }

  if (cell.value === X_TURN) {
    if (cell.element.classList.contains(X_TURN)) {
      return;
    }
    if (cell.element.classList.contains(O_TURN)) {
      return cell.element.classList.remove(
        getCorrectClassByCurrentTurn(O_TURN)
      );
    }

    return cell.element.classList.add(getCorrectClassByCurrentTurn(X_TURN));
  }

  if (cell.value === O_TURN) {
    if (cell.element.classList.contains(O_TURN)) {
      return;
    }
    if (cell.element.classList.contains(X_TURN)) {
      return cell.element.classList.remove(
        getCorrectClassByCurrentTurn(X_TURN)
      );
    }

    return cell.element.classList.add(getCorrectClassByCurrentTurn(O_TURN));
  }
}

function setActiveTurn(activeTurn) {
  currentTurnElement$.innerText = `Ход: ${activeTurn}`;
}

function setFinishingBlock(winner) {
  if (!winner) {
    return (winnerElement$.innerText = `Игра окончена. Результат: ничья`);
  }
  return (winnerElement$.innerText = `Игра окончена. Победитель: ${winner}`);
}

function unsetFinishingBLock() {
  winnerElement$.innerText = "";
}

function setGameBoardDisabled() {
  rootElement$.classList.add(TIC_TAC_TOE_DISABLED_CLASS);
}

function setGameBoardEnabled() {
  rootElement$.classList.remove(TIC_TAC_TOE_DISABLED_CLASS);
}

function doesNeedToUpdateCell(prevCell, newCell) {
  return Boolean(prevCell.value !== newCell.value);
}

function initElements() {
  rootElement$ = createTicTacToeElement();

  gameBoardElement$ = rootElement$.querySelector(
    TIC_TAC_TOE_GAME_BOARD_SELECTOR
  );
  currentTurnElement$ = rootElement$.querySelector(CURRENT_TURN_SELECTOR);
  winnerElement$ = rootElement$.querySelector(WINNER_BLOCK_SELECTOR);
  startNewGameButton$ = rootElement$.querySelector(
    START_NEW_GAME_BUTTON_SELECTOR
  );
}

function initCellListener(currentCell) {
  const cellElement$ = currentCell.element;

  cellElement$.addEventListener("click", () => {
    cellMarkedHandler(currentCell);
  });
  gameBoardElement$.appendChild(cellElement$);
}

function initCellsListeners(cells) {
  cells.forEach(initCellListener);
}

function initialRender(store) {
  initElements();

  initNewStartGameButtonListener(store);
  initCellsListeners(store.cells);
  setActiveTurn(store.currentTurn);

  store.appTarget.replaceWith(rootElement$);
}

function saveNewState(newState) {
  currentState = {
    ...newState,
    cells: newState.cells.map((cell) => ({ ...cell })),
  };
}

function render(newState) {
  if (currentState === null) {
    initialRender(newState);
    saveNewState(newState);
    return;
  }

  newState.cells.forEach((currentCell, ind) => {
    if (
      currentState.cells.every((cell) => cell.element !== currentCell.element)
    ) {
      initCellListener(currentCell);
      updateCellView(currentCell, X_TURN, O_TURN);
    } else {
      if (doesNeedToUpdateCell(currentState.cells[ind], currentCell)) {
        updateCellView(currentCell, X_TURN, O_TURN);
      }
    }
  });

  if (currentState.currentTurn !== newState.currentTurn) {
    setActiveTurn(newState.currentTurn);
  }

  if (newState.finishingData.isFinished) {
    setFinishingBlock(newState.finishingData.winner);
    setGameBoardDisabled();
  }

  saveNewState(newState);
}

export { render };
