import { initGame } from "./services.js";

const appTargetElement$ = document.querySelector(".tic_tac_toe");

initGame(appTargetElement$);
